var menuState = {
    create: function () {
        console.log('[menuState]: display menu');

        var baImg = game.add.image(0, 0, 'strat_bg');
        baImg.width = game.width;
        baImg.height = game.height;

        // var nameLabel = game.add.text(game.width / 2, 80, 'Game of Thrones', {
        //     font: '50px Arial',
        //     fill: '#ffffff'
        // });
        // nameLabel.anchor.setTo(0.5, 0.5);

        // var scoreLabel = game.add.text(game.width / 2, game.height / 2,
        //     'score: ' + game.global.score, {
        //         font: '25px Arial',
        //         fill: '#ffffff'
        //     });
        // scoreLabel.anchor.setTo(0.5, 0.5);

        var startLabel = game.add.text(game.width / 2, game.height - 80,
            'press the up arrow key to start', {
                font: '25px Arial',
                fill: '#ffffff'
            });
        startLabel.anchor.setTo(0.5, 0.5);

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);
    },
    start: function () {
        game.state.start('hall');
    },
};