class EnemyObj {
    constructor(
        spriteName,
        life,
        atk,
        weapon,
        bullet,
        game,
        initPos,
        group = null,
        bulletGroup = null,
        targetSprite = null
    ) {
        this.game = game;
        this.sprite = this.game.add.sprite(
            initPos.x,
            initPos.y,
            spriteName,
            0,
            group
        );
        // this.game.world.addAt(this.sprite, 1);
        this.life = life;
        this.atk = atk;
        this.weapon = this.game.add.sprite(0, 0, weapon);
        // this.sprite.z = 5;
        this.sprite.anchor.setTo(0.5, 0.5);
        this.sprite.addChild(this.weapon);

        this.sprite.wraper = this;
        // console.log(this.sprite);

        this.sprite.enableBody = true;
        game.physics.arcade.enable(this.sprite);
        this.sprite.physicsBodyType = Phaser.Physics.ARCADE;
        this.sprite.collideWorldBounds = true;

        this.bullet = this.game.add.weapon(5, bullet, 0, bulletGroup);
        this.bullet.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet.bulletSpeed = 600;
        this.bullet.fireRate = 1000;
        this.bullet.trackSprite(this.sprite, 0, 0, true);

        this.bullet.bullets.forEach(this._initBulletAtk, this);

        // this.bullet.autofire = true;
        this.bullet.enableBody = true;
        game.physics.arcade.enable(this.bullet);
        this.bullet.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullet.collideWorldBounds = true;

        this._moveTimer = this.game.time.create(false);
        this._moveTimer.loop(800, this._pickDirection, this);
        this._moveTimer.start();
        this._attackTimer = this.game.time.create(false);
        this._attackTimer.loop(2000, this.attack, this);
        this._attackTimer.start();

        this._hurtLabel = game.add.text(game.width / 2, 80, "3", {
            font: "40px Geo",
            fill: "#ff0000"
        });
        this._hurtLabel.visible = false;
        this._targetSprite = targetSprite;
    }

    attack(followSprit = true) {
        console.log("attack");
        if (
            Math.sqrt(
                Math.pow(this._targetSprite.x - this.sprite.x, 2) +
                    Math.pow(this._targetSprite.y - this.sprite.y, 2)
            ) < 500
        ) {
            if (followSprit)
                this.bullet.fire(
                    null,
                    this._targetSprite.x,
                    this._targetSprite.y
                );
            else this.bullet.fire();
        }
    }

    hurt(val = 0) {
        game.time.events.add(200, this._resetLabel, this, null);
        this._hurtLabel.position.set(this.sprite.x + 5, this.sprite.y + 5);
        this._hurtLabel.text = val.toString();
        this._hurtLabel.visible = true;
        this.life -= val;
        if (this.life < 0) {
            this.die();
            console.log(enemy.wraper);
            // delete enemy.wraper;
        } else {
            game.time.events.add(100, this._resetTint, this, null);
            this.sprite.tint = 0xff0000;
        }
    }

    _initBulletAtk(e) {
        e.atk = this.atk;
    }

    _pickDirection() {
        this.sprite.body.velocity.x = 100 * game.rnd.pick([-1, 1, 0, 0, 0]);
        this.sprite.body.velocity.y = 100 * game.rnd.pick([-1, 1, 0, 0, 0]);
        // console.log(this.sprite.position)
        if (this.sprite.body.velocity.x < 0) this.sprite.scale.x = -1;
        else this.sprite.scale.x = 1;
    }

    _resetLabel() {
        this._hurtLabel.visible = false;
    }

    _resetTint() {
        if (this.sprite) this.sprite.tint = 0xffffff;
    }
    die() {
        this._moveTimer.removeAll();
        this._attackTimer.removeAll();
        this.sprite.kill();
        this.bullet.destroy();
        delete this.weapon;
        delete this.sprite;
    }
}

class SkeletonBow {
    constructor(
        spriteName,
        life,
        atk,
        weapon,
        bullet,
        game,
        initPos,
        group = null,
        bulletGroup = null,
        targetSprite = null
    ) {
        this.game = game;
        this.sprite = this.game.add.sprite(
            initPos.x,
            initPos.y,
            spriteName,
            0,
            group
        );
        // this.game.world.addAt(this.sprite, 1);
        this.life = life;
        this.atk = atk;
        this.weapon = this.game.add.sprite(0, 0, weapon);
        this.weapon.scale.setTo(0.5);
        // this.sprite.z = 5;
        this.sprite.anchor.setTo(0.5, 0.5);
        this.sprite.addChild(this.weapon);

        this.sprite.wraper = this;
        // console.log(this.sprite);

        this.sprite.enableBody = true;
        game.physics.arcade.enable(this.sprite);
        this.sprite.physicsBodyType = Phaser.Physics.ARCADE;
        this.sprite.collideWorldBounds = true;

        this.bullet = this.game.add.weapon(5, bullet, 0, bulletGroup);
        this.bullet.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet.bulletSpeed = 600;
        this.bullet.fireRate = 1000;
        this.bullet.trackSprite(this.sprite, 0, 0, true);
        // this.bullet.bullets.setAllChildren('scale', 0.5);

        this.bullet.bullets.forEach(this._initBulletAtk, this);

        // this.bullet.autofire = true;
        this.bullet.enableBody = true;
        game.physics.arcade.enable(this.bullet);
        this.bullet.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullet.collideWorldBounds = true;

        this._moveTimer = this.game.time.create(false);
        this._moveTimer.loop(800, this._pickDirection, this);
        this._moveTimer.start();
        this._attackTimer = this.game.time.create(false);
        this._attackTimer.loop(2000, this.attack, this);
        this._attackTimer.start();

        this._hurtLabel = game.add.text(game.width / 2, 80, "3", {
            font: "40px Geo",
            fill: "#ff0000"
        });
        this._hurtLabel.visible = false;
        this._targetSprite = targetSprite;
        this.sprite.animations.add("walk", [1, 2, 3], 8, true);
    }

    attack(followSprit = true) {
        console.log("attack");
        if (
            Math.sqrt(
                Math.pow(this._targetSprite.x - this.sprite.x, 2) +
                    Math.pow(this._targetSprite.y - this.sprite.y, 2)
            ) < 500
        ) {
            if (followSprit)
                this.bullet.fire(
                    null,
                    this._targetSprite.x,
                    this._targetSprite.y
                );
            else this.bullet.fire();
        }
    }

    hurt(val = 0) {
        game.time.events.add(200, this._resetLabel, this, null);
        this._hurtLabel.position.set(this.sprite.x + 5, this.sprite.y + 5);
        this._hurtLabel.text = val.toString();
        this._hurtLabel.visible = true;
        this.life -= val;
        if (this.life < 0) {
            this.die();
            console.log(enemy.wraper);
            // delete enemy.wraper;
        } else {
            game.time.events.add(100, this._resetTint, this, null);
            this.sprite.tint = 0xff0000;
        }
    }

    _initBulletAtk(e) {
        e.atk = this.atk;
    }

    _pickDirection() {
        this.sprite.body.velocity.x = 100 * game.rnd.pick([-1, 1, 0, 0, 0]);
        this.sprite.body.velocity.y = 100 * game.rnd.pick([-1, 1, 0, 0, 0]);
        // console.log(this.sprite.position)
        if (this.sprite.body.velocity.x < 0) this.sprite.scale.x = -1;
        else this.sprite.scale.x = 1;
        this.sprite.animations.play("walk");
        if (
            this.sprite.body.velocity.x == 0 &&
            this.sprite.body.velocity.y == 0
        )
            this.sprite.animations.stop();
    }

    _resetLabel() {
        this._hurtLabel.visible = false;
    }

    _resetTint() {
        if (this.sprite) this.sprite.tint = 0xffffff;
    }
    die() {
        this._moveTimer.removeAll();
        this._attackTimer.removeAll();
        this.sprite.kill();
        this.bullet.destroy();
        delete this.weapon;
        delete this.sprite;
    }
}

class BigSkeleton {
    constructor(
        spriteName,
        life,
        atk,
        weapon,
        bullet,
        game,
        initPos,
        group = null,
        bulletGroup = null,
        targetSprite = null
    ) {
        this.game = game;
        this.sprite = this.game.add.sprite(
            initPos.x,
            initPos.y,
            spriteName,
            0,
            group
        );
        // this.game.world.addAt(this.sprite, 1);
        this.life = life;
        this.atk = atk;
        this.weapon = this.game.add.sprite(0, 0, weapon);
        this.weapon.anchor.setTo(0, 0);
        this.swapAnimation = this.game.add.sprite(0, 0, "attackss");
        this.swapAnimation.scale.setTo(0.4);
        this.swapAnimation.anchor.setTo(0.5, 0.8);
        this.swapAnimation.animations.add("swap", [0, 1, 2, 3], 5, false);
        // this.sprite.z = 5;
        this.sprite.anchor.setTo(0.5, 0.5);
        this.sprite.addChild(this.weapon);
        this.sprite.addChild(this.swapAnimation);
        // this.sprite.scale.setTo(5);

        this.sprite.wraper = this;
        // console.log(this.sprite);

        this.sprite.enableBody = true;
        game.physics.arcade.enable(this.sprite);
        this.sprite.physicsBodyType = Phaser.Physics.ARCADE;
        this.sprite.collideWorldBounds = true;

        console.log(bulletGroup);
        this.bullet = this.game.add.sprite(0, 0, "attackss", 0, bulletGroup);
        bulletGroup.add(this.bullet);
        this.bullet.enableBody = true;
        game.physics.arcade.enable(this.bullet);
        this.bullet.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullet.scale.setTo(0.4);
        this.bullet.anchor.setTo(0.5, 0.8);
        this.bullet.visible = false;
        this.sprite.addChild(this.bullet);
        this.bullet.atk = this.atk;

        this._moveTimer = this.game.time.create(false);
        this._moveTimer.loop(800, this._pickDirection, this);
        this._moveTimer.start();
        this._attackTimer = this.game.time.create(false);
        this._attackTimer.loop(5000, this.attack, this);
        this._attackTimer.start();

        this._hurtLabel = game.add.text(game.width / 2, 80, "3", {
            font: "40px Geo",
            fill: "#ff0000"
        });
        this._hurtLabel.visible = false;
        this._targetSprite = targetSprite;
        this.sprite.animations.add("walk", [1, 2, 3, 4], 8, true);
    }

    attack(followSprit = true) {
        game.add
            .tween(this.weapon)
            .to({ angle: -90 }, 500)
            .start();

        this.swapAnimation.visible = true;
        this.swapAnimation.animations.play("swap");
        this.swapAnimation.animations.currentAnim.onComplete.addOnce(
            function() {
                this.swapAnimation.visible = false;
            },
            this
        );
        game.add
            .tween(this.weapon)
            .to({ angle: 0 }, 100)
            .start();

        this.bullet.alive = true;
    }

    hurt(val = 0) {
        game.time.events.add(200, this._resetLabel, this, null);
        this._hurtLabel.position.set(this.sprite.x + 5, this.sprite.y + 5);
        this._hurtLabel.text = val.toString();
        this._hurtLabel.visible = true;
        this.life -= val;
        if (this.life < 0) {
            this.die();
            console.log(enemy.wraper);
            // delete enemy.wraper;
        } else {
            game.time.events.add(100, this._resetTint, this, null);
            this.sprite.tint = 0xff0000;
        }
    }

    _pickDirection() {
        this.sprite.body.velocity.x = 100 * game.rnd.pick([-1, 1, 0, 0, 0]);
        this.sprite.body.velocity.y = 100 * game.rnd.pick([-1, 1, 0, 0, 0]);
        // console.log(this.sprite.position)
        if (this.sprite.body.velocity.x < 0) this.sprite.scale.x = -1;
        else this.sprite.scale.x = 1;
        this.sprite.animations.play("walk");
        if (
            this.sprite.body.velocity.x == 0 &&
            this.sprite.body.velocity.y == 0
        )
            this.sprite.animations.stop();
    }

    _resetLabel() {
        this._hurtLabel.visible = false;
    }

    _resetTint() {
        if (this.sprite) this.sprite.tint = 0xffffff;
    }
    die() {
        this._moveTimer.removeAll();
        this._attackTimer.removeAll();
        this.sprite.kill();
        this.bullet.destroy();
        delete this.weapon;
        delete this.sprite;
    }
}

class BossObj {
    constructor(
        spriteName,
        life,
        atk,
        weapon,
        bullet,
        game,
        initPos,
        group = null,
        bulletGroup = null,
        targetSprite = null
    ) {
        this.game = game;
        this.sprite = this.game.add.sprite(
            initPos.x,
            initPos.y,
            spriteName,
            0,
            group
        );
        // this.game.world.addAt(this.sprite, 1);
        this.life = life;
        this.atk = atk;
        this.weapon = this.game.add.sprite(0, 0, weapon, 0, group);
        this.weapon.anchor.setTo(0, 0);
        this.swapAnimation = this.game.add.sprite(0, 0, "attackss",0,group);
        this.swapAnimation.scale.setTo(0.4);
        this.swapAnimation.anchor.setTo(0.5, 0.8);
        this.swapAnimation.animations.add("swap", [0, 1, 2, 3], 5, false);
        // this.sprite.z = 5;
        this.sprite.anchor.setTo(0.5, 0.5);
        this.sprite.addChild(this.weapon);
        this.sprite.addChild(this.swapAnimation);
        // this.sprite.scale.setTo(5);

        this.sprite.wraper = this;
        // console.log(this.sprite);

        this.sprite.enableBody = true;
        game.physics.arcade.enable(this.sprite);
        this.sprite.physicsBodyType = Phaser.Physics.ARCADE;
        this.sprite.collideWorldBounds = true;

        this.bullet = this.game.add.weapon(5, bullet, 0, bulletGroup);
        this.bullet.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet.bulletSpeed = 1000;
        this.bullet.trackSprite(this.sprite, 0, 0, true);

        this._moveTimer = this.game.time.create(false);
        this._moveTimer.loop(500, this._pickDirection, this);
        this._moveTimer.start();
        this._attackTimer = this.game.time.create(false);
        this._attackTimer.loop(1000, this.attack, this);
        this._attackTimer.start();

        this._hurtLabel = game.add.text(game.width / 2, 80, "3", {
            font: "40px Geo",
            fill: "#ff0000"
        });
        this._hurtLabel.visible = false;
        this._targetSprite = targetSprite;
        this.sprite.animations.add("walk", [1, 2, 3, 4], 8, true);
    }

    attack(followSprit = true) {
        // this.weapon.
        game.add
            .tween(this.weapon)
            .to({ angle: -90 }, 500)
            .start();

        this.swapAnimation.visible = true;
        this.swapAnimation.animations.play("swap");
        this.swapAnimation.animations.currentAnim.onComplete.addOnce(
            function() {
                this.swapAnimation.visible = false;
            },
            this
        );
        game.add
            .tween(this.weapon)
            .to({ angle: 0 }, 100)
            .start();

        // if (
        //     Math.sqrt(
        //         Math.pow(this._targetSprite.x - this.sprite.x, 2) +
        //             Math.pow(this._targetSprite.y - this.sprite.y, 2)
        //     ) < 500
        // ) {
        //     if (followSprit)
        //         this.bullet.fire(
        //             null,
        //             this._targetSprite.x,
        //             this._targetSprite.y
        //         );
        //     else this.bullet.fire();
        // }
    }

    hurt(val = 0) {
        game.time.events.add(200, this._resetLabel, this, null);
        this._hurtLabel.position.set(this.sprite.x + 5, this.sprite.y + 5);
        this._hurtLabel.text = val.toString();
        this._hurtLabel.visible = true;
        this.life -= val;
        if (this.life < 0) {
            this.die();
            console.log(enemy.wraper);
            game.state.start("Win");
            // delete enemy.wraper;
        } else {
            game.time.events.add(100, this._resetTint, this, null);
            this.sprite.tint = 0xff0000;
        }
    }

    _pickDirection() {
        //speed up the boss
        this.sprite.body.velocity.x = 80 * game.rnd.pick([-1, 1, 4, -6, 3, 9, -10]);
        this.sprite.body.velocity.y = 50 * game.rnd.pick([-1, 1, -6, 5, 4 , -9 , 10]);
        // console.log(this.sprite.position)
        if (this.sprite.body.velocity.x < 0) this.sprite.scale.x = -1;
        else this.sprite.scale.x = 1;
        // this.sprite.animations.play("walk");
        if (
            this.sprite.body.velocity.x == 0 &&
            this.sprite.body.velocity.y == 0
        )
            this.sprite.animations.stop();
    }

    _resetLabel() {
        this._hurtLabel.visible = false;
    }

    _resetTint() {
        if (this.sprite) this.sprite.tint = 0xffffff;
    }
    die() {
        this._moveTimer.removeAll();
        this._attackTimer.removeAll();
        this.sprite.kill();
        this.bullet.destroy();
        delete this.weapon;
        delete this.sprite;
    }
}
