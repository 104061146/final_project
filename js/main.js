var game = new Phaser.Game(1280, 800, Phaser.AUTO, 'canvas');
game.global = { score: 0, player: undefined, volume:0.5};
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('hall', hallState);
game.state.add('play', playState);
game.state.add('playEnemy', playEnemyState);
game.state.add('playTest', playTestState);
game.state.add('Win', WinState);

// Start the 'boot' state
game.state.start('boot');