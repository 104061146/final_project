var loadState = {
    preload: function() {
        console.log("[loadState]: load assets for menuState");

        var loadingLabel = game.add.text(game.width / 2, 150, "loading...", {
            font: "30px Arial",
            fill: "#ffffff"
        });
        loadingLabel.anchor.setTo(0.5, 0.5);

        // Display the progress bar
        var progressBar = game.add.sprite(game.width / 2, 200, "progressBar");
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);

        // Load all game assets
        game.load.audio("opening", "assets/audio/opening.wav");
        game.load.image("strat_bg", 'assets/start_meng.png');

        // Load characters
        game.load.image("character1", "./assets/characters/wizard.png");
        game.load.image("character2", "./assets/characters/elf.png");
        game.load.image("character3", "./assets/characters/paladin.png");
        game.load.image("character4", "./assets/characters/assassin.png");
        game.load.image("pixel", "./assets/attacks/pixel.png");
        game.load.image("Win", "./assets/test/Win.jpg");


        // Load enemies
        game.load.image("enemy1", "./assets/enemies/bigSkeleton.png");
        game.load.spritesheet("enemy1ss", "./assets/enemies/spritesheet.png", 94,82);
        game.load.image("enemy2", "./assets/enemies/eliteSkeletonSword.png");
        game.load.image("enemy3", "./assets/enemies/skeletonBow.png");
        game.load.spritesheet("enemy3ss", "./assets/enemies/skbss.png",57,52);
        game.load.image("enemy4", "./assets/enemies/skeletonSword.png");
        game.load.image("enemy5", "./assets/enemies/Sprite_dragonblack.png");


        // Load enemy attack assets
        game.load.image("enemyArrow", "./assets/attacks/arrowEnemy.png");
        game.load.image("enemyShock", "./assets/attacks/areaAttack.png");
        game.load.image("enemyIcePulse", "./assets/attacks/icePulse.png");
        game.load.spritesheet("attackss", "./assets/attacks/attackss.png", 512, 512);

        // Load weapons
        game.load.image('weapon1', './assets/weapons/bow.png');
        game.load.image('weapon2', './assets/weapons/sword.png');
        game.load.image('weapon3', './assets/weapons/greatSword.png');
        game.load.image('weapon4', './assets/weapons/flameStaff.png');
        game.load.image('weapon5', './assets/weapons/lightStaff.png');
        game.load.image('weapon6', './assets/weapons/axe.png');
        game.load.image('weapon7', './assets/weapons/ice.png');
        game.load.image('weapon8', './assets/weapons/needle.png');
        game.load.image('weapon9', './assets/weapons/snow.png');
        game.load.image('weapon10', './assets/weapons/ice.png');
        game.load.image('weapon11', './assets/weapons/goldsword.png');
        game.load.image('weapon12', './assets/weapons/firesword.png');
        game.load.image('weapon13', './assets/weapons/SpriteThunderWarhammer.png');






        //
        //game.load.spritesheet('cut','./assets/attacks/sword/ice/ice_a.png',240, 240);
        game.load.spritesheet('cut','./assets/attacks/cut.png',519, 432);
        game.load.spritesheet('flame','./assets/attacks/flame.png',512, 512);
        game.load.spritesheet('stick','./assets/attacks/stick1.png',180, 120);

        game.load.spritesheet('Box','./assets/test/treasureBox.png',35, 35);




        game.load.spritesheet("cut", "./assets/attacks/cut.png", 519, 432);
        game.load.spritesheet("flame", "./assets/attacks/flame.png", 512, 512);

        //Load sp
        game.load.image("shield", "./assets/sp/shield__.png");
        game.load.image("fire_ball", "assets/sp/fire_ball.png");
        game.load.image("sp_shield", "assets/sp/spr_shield.png");
        game.load.spritesheet("speed", "assets/sp/picture.png", 512, 512);
        game.load.spritesheet("dragon", "assets/sp/dragon.png", 144, 128);
        game.load.spritesheet("skeleton", "assets/sp/skeleton.png", 16, 16);
        game.load.spritesheet("ghost", "assets/sp/ghost.png", 16, 16);
        game.load.spritesheet(
            "WitchEffect",
            "assets/sp/WitchEffect.png",
            320,
            120
        );

        // Load a new asset that we will use in the menu state
        game.load.image("menuBg", "./assets/menu_bg.png");
        game.load.image("defaultBg", "./assets/default_bg.png");
        //
        //game.load.image('bullet1', './assets/attacks/icePulse.png');
        game.load.image("bullet1", "./assets/attacks/fireball.png");

        game.load.spritesheet('bullet5', './assets/attacks/arrow.png',48,48);
        game.load.image('bullet2', './assets/attacks/arrowEnemy.png');
        game.load.image('bullet3', './assets/attacks/firesword.png');
        game.load.image('bullet4', './assets/attacks/icebullet.png');


        game.load.image("bullet2", "./assets/attacks/arrowEnemy.png");

        //load tile and tilemap(test)
        game.load.image("tileset", "./assets/test/tileset1.png");
        game.load.image("tileset1", "./assets/test/snowtileset.png");
        game.load.tilemap(
            "map",
            "./assets/test/map.json",
            null,
            Phaser.Tilemap.TILED_JSON
        );
        game.load.image("tileset2", "./assets/test/tileset2.png");
        game.load.tilemap(
            "map2",
            "./assets/test/map2.json",
            null,
            Phaser.Tilemap.TILED_JSON
        );
        game.load.tilemap(
            "bigMap",
            "./assets/test/bigMap.json",
            null,
            Phaser.Tilemap.TILED_JSON
        );

        //
        //game.load.image("bullet1", "./assets/attacks/icePulse.png");
        //game.load.image("bullet2", "./assets/attacks/fireball.png");

        //load tile and tilemap(test)
        game.load.image("tileset", "./assets/test/tileset1.png");
        game.load.image("tileset1", "./assets/test/snowtileset.png");
        game.load.tilemap(
            "map",
            "./assets/test/map.json",
            null,
            Phaser.Tilemap.TILED_JSON
        );
        game.load.image("tileset2", "./assets/test/tileset2.png");
        game.load.tilemap(
            "map2",
            "./assets/test/map2.json",
            null,
            Phaser.Tilemap.TILED_JSON
        );
        game.load.spritesheet("door", "assets/test/door.png", 320, 120);
        game.load.spritesheet("laser","assets/test/laser.png",512,128);

        //load treasure box
        game.load.spritesheet("box","assets/test/treasureBox.png",35,35);
    },
    create: function() {
        console.log("[loadState]: state finish -> switch to menuState");
        game.state.start("menu");
    }
};
