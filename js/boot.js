var bootState = {
    preload: function () {
        // Load the progress bar image.
        console.log('[bootState]: load assets for loadState');
        game.load.image('progressBar', 'assets/progressBar.png');
    },
    create: function () {
        // Set some game settings. 
        game.stage.backgroundColor = '#303030';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        console.log('[bootState]: state finish -> switch to loadState');
        game.state.start('load');
    }
};