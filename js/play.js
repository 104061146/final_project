// game.physics.arcade.collide(this.player, this.wall);
// game.physics.arcade.collide(this.player, this.obstacle);

var playState = {
    preload: function() {
        game.stage.backgroundColor = "#303030";
    },
    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        this._setupKeys();
        this.bulletTime = 0;
        this._initTilemap1();
        this.enemyGroup = game.add.group();
        this.enemyBulletGroup = game.add.group();
        this.enemySmashGroup = game.add.group();
        // this.enemyGroup.bringToTop();
        this.player.enemyGroup.push(this.enemyGroup);
        this.box = new Box("Box", game, this.enemyGroup,3692,573);
        this.box1 = new Box("Box", game, this.enemyGroup,2321,3838);

        this.cursor = game.input.keyboard.createCursorKeys();

        this._initEnemy();
        console.log(this.enemySmashGroup.children);
        this.timeout=0;
        // console.log(this.enemyBulletGroup);
    },
    update: function() {
        game.physics.arcade.collide(this.player.character, this.wall);

        game.physics.arcade.collide(this.enemyGroup, this.wall);
        game.physics.arcade.collide(this.enemyGroup, this.invisible);
        game.physics.arcade.collide(this.enemyGroup, this.obstacle);
        game.physics.arcade.collide(
            this.enemySmashGroup,
            this.player.character,
            this.player.playerHurt,
            null,
            this.player
        );

        this.enemyBulletGroup.forEachAlive(this._enemyCollide, this);

        this.player.movePlayer();
        this.player.checkSP();
        this.box.movePlayer();
        this.box1.movePlayer();
        if(game.time.now>this.timeout){
            this.timeout=game.time.now+200;
            if(this.player.mp<this.player.fullmp)
            {//this.player.mp+=1;
             this.player.changePower(1);
            }

        }
        // this.box.movePlayer();
        // console.log(this.player.character.position);
        game.physics.arcade.overlap(
            this.player.weapon.bullet,
            this.enemyGroup,
            this._enemyHurt,
            null,
            this
        );
        game.physics.arcade.overlap(
            this.player.character,
            this.box.character,
            this.player.pickUpWeapon,
            null,
            this.player
        );
        game.physics.arcade.overlap(
            this.player.character,
            this.box1.character,
            this.player.pickUpWeapon,
            null,
            this.player
        );
        if (this.player.sheldflage != undefined) {
            game.physics.arcade.overlap(
                this.player.effect2,
                this.enemyBulletGroup,
                this.player.deffense,
                null,
                this
            );
        }

        if (this.player.fireball != undefined) {
            // console.log(this.player.fireball.bullets);
            game.physics.arcade.overlap(
                this.player.fireball.bullets,
                this.enemyGroup,
                this._enemyHurt,
                null,
                this
            );
        }
        

        if (fireButton.isDown) {
            if (game.time.now > this.bulletTime) {
                this.bulletTime = game.time.now + 500;
                if (this.player.check_fire) {
                    this.player.fireFn();
                } else {
                    console.log("fire!!!");
                    if(this.player.mp>this.player.weapon.energy){
                    this.player.weapon.attack();
                    this.player.changePower(-this.player.weapon.energy);
                    }
                }
            }
        }

        if (
            this.nextzone.contains(
                this.player.character.x + this.player.character.width / 2,
                this.player.character.y + this.player.character.height / 2
            )
        ) {
            this.laser.alpha = 1;
            game.physics.arcade.collide(this.player.character, this.invisible);
        }

        if (
            this.nextzone2.contains(
                this.player.character.x + this.player.character.width / 2,
                this.player.character.y + this.player.character.height / 2
            )
        ) {
            // alert("i am in");
            this.laser2.alpha = 1;
            game.physics.arcade.collide(this.player.character, this.invisible);
            // alert("To the next room!");
        }

        if (
            this.nextzone3.contains(
                this.player.character.x + this.player.character.width / 2,
                this.player.character.y + this.player.character.height / 2
            )
        ) {
            // alert("i am in");
            this.laser3.alpha = 1;
            game.physics.arcade.collide(this.player.character, this.invisible);
        }

        if (
            this.nextzone4.contains(
                this.player.character.x + this.player.character.width / 2,
                this.player.character.y + this.player.character.height / 2
            )
        ) {
            // alert("i am in");
            this.laser4.alpha = 1;
            game.physics.arcade.collide(this.player.character, this.invisible);
        }
    },
    _enemyCollide: function(c) {
        game.physics.arcade.collide(c, this.wall, this._killEnemyBullet, null, this);
        game.physics.arcade.collide(c, this.obstacle, this._killEnemyBullet, null, this);
        game.physics.arcade.collide(
            c,
            this.player.character,
            this.player.playerHurt,
            null,
            this.player
        );
        if (this.player.sheldflage != undefined) {
            game.physics.arcade.collide(
                this.player.effect2,
                c,
                this.player.deffense,
                null,
                this
            );
            game.physics.arcade.collide(
                this.player.effect,
                c,
                this.player.deffense,
                null,
                this
            );
        }
    },

    _killEnemyBullet: function(bullet, others) {
        bullet.kill();
    },

    _selectCharacter: function() {
        if (game.global.player == "character1")
            this.player = new CharacterMelisandre(game);
        else if (game.global.player == "character2")
            this.player = new CharacterDaenerys(game);
        else if (game.global.player == "character3")
            this.player = new CharacterJon(game);
        else this.player = new CharacterArya(game);
    },
    _setupKeys: function() {
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    },
    _enemyHurt: function(bullets, enemy) {
        console.log("enemy hurt");
        console.log(bullets);
        if (this.player.fireball!=undefined){
            if(this.player.check_fire)
            {enemy.wraper.hurt(this.player.fireball.damage);}
            else{
                enemy.wraper.hurt(this.player.weapon.bullet.wraper.damage);
            }
        }


        else
        {
            enemy.wraper.hurt(this.player.weapon.bullet.wraper.damage);
            if (this.player.weapon.activateAnimation)
                this.player.weapon.activateAnimation(bullets, enemy);
            // if (enemy.wraper.life < 0) {
            //     enemy.wraper.die();
            //     console.log(enemy.wraper);
            //     delete enemy.wraper;
            // }
            bullets.kill();
        }
    },
    _initTilemap1: function() {
        // Create the tilemap
        this.map = game.add.tilemap("bigMap");

        // Add the tileset to the map
        this.map.addTilesetImage("tileset");
        this.map.addTilesetImage("tileset1");
        this.map.addTilesetImage("tileset2");

        // Create the layer by specifying the name of the Tiled layer
        this.bg = this.map.createLayer("background");
        this.wall = this.map.createLayer("wall");
        this.invisible = this.map.createLayer("invisible");

        //this.obstacle = this.map.createLayer('obstacles');

        this.bg.renderSettings.enableScrollDelta = true;
        this.wall.renderSettings.enableScrollDelta = true;
        this.invisible.renderSettings.enableScrollDelta = true;
        // this.invisible2.renderSettings.enableScrollDelta = true;

        this.game.physics.arcade.enable(this.map);
        this.game.physics.arcade.enable(this.wall);
        this.game.physics.arcade.enable(this.invisible);
        // this.game.physics.arcade.enable(this.invisible2);
        //game.physics.arcade.enable(this.obstacle);

        // Set the world size to match the size of the layer
        this.bg.resizeWorld();
        this.wall.resizeWorld();
        this.invisible.resizeWorld();
        // this.invisible2.resizeWorld();

        this._selectCharacter();
        this.game.world.addAt(this.player.character, 1);

        //add collision
        this.map.setCollisionBetween(1, 1000, true, this.wall);
        this.map.setCollisionBetween(1, 1000, true, this.invisible);
        // this.map.setCollisionBetween(1, 1000, true, this.invisible2);
        //this.map.setCollisionBetween(1, 1000, true, this.obstacle);

        // this._selectCharacter();
        this.player.character.enableBody = true;
        this.player.character.collideWorldBounds = true;

        //set camera
        game.camera.follow(this.player.character);

        //create starting point and ending point
        this.tilemapStart = this.map.objects["GameObjects"][0];
        this.tilemapEnd = this.map.objects["GameObjects"][1];
        this.tilemaplaser = this.map.objects["GameObjects"][2];
        this.tilemaplaser2 = this.map.objects["GameObjects"][3];
        this.tilemapEnd2 = this.map.objects["GameObjects"][4];
        this.tilemaplaser3 = this.map.objects["GameObjects"][5];
        this.tilemapEnd3 = this.map.objects["GameObjects"][6];
        this.tilemaplaser4 = this.map.objects["GameObjects"][7];
        this.tilemapEnd4 = this.map.objects["GameObjects"][8];

        this.treasure = this.map.objects["GameObjects"][9];

        this.laser = game.add.sprite(
            this.tilemaplaser.x + 75,
            this.tilemaplaser.y - 30,
            "laser"
        );
        this.laser.alpha = 0;
        this.laser.scale.setTo(0.5, 0.2);
        this.laser.angle = 90;
        this.laser.enableBody = true;
        this.game.physics.arcade.enable(this.laser);
        this.laser.physicsBodyType = Phaser.Physics.ARCADE;
        this.laser.immovable = true;

        this.laser2 = game.add.sprite(
            this.tilemaplaser2.x + 40,
            this.tilemaplaser2.y + 90,
            "laser"
        );
        this.laser2.alpha = 0;
        this.laser2.scale.setTo(0.7, 0.2);
        this.laser2.angle = 180;
        this.laser2.enableBody = true;
        this.game.physics.arcade.enable(this.laser2);
        this.laser2.physicsBodyType = Phaser.Physics.ARCADE;
        this.laser2.immovable = true;

        this.laser3 = game.add.sprite(
            this.tilemaplaser3.x + 10,
            this.tilemaplaser3.y - 70,
            "laser"
        );
        this.laser3.alpha = 0;
        this.laser3.scale.setTo(0.7, 0.2);
        this.laser3.angle = 90;
        this.laser3.enableBody = true;
        this.game.physics.arcade.enable(this.laser3);
        this.laser3.physicsBodyType = Phaser.Physics.ARCADE;
        this.laser3.immovable = true;

        this.laser4 = game.add.sprite(
            this.tilemaplaser4.x + 325,
            this.tilemaplaser4.y + 20,
            "laser"
        );
        this.laser4.alpha = 0;
        this.laser4.scale.setTo(0.7, 0.2);
        this.laser4.angle = 180;
        this.laser4.enableBody = true;
        this.game.physics.arcade.enable(this.laser4);
        this.laser4.physicsBodyType = Phaser.Physics.ARCADE;
        this.laser4.immovable = true;

        this.player.character.position.set(
            this.tilemapStart.x,
            this.tilemapStart.y
        );

        //To the next room
        this.nextzone = new Phaser.Rectangle(
            this.tilemapEnd.x,
            this.tilemapEnd.y,
            this.tilemapEnd.width,
            this.tilemapEnd.height
        );

        this.nextzone2 = new Phaser.Rectangle(
            this.tilemapEnd2.x,
            this.tilemapEnd2.y,
            this.tilemapEnd2.width,
            this.tilemapEnd2.height
        );

        this.nextzone3 = new Phaser.Rectangle(
            this.tilemapEnd3.x,
            this.tilemapEnd3.y,
            this.tilemapEnd3.width,
            this.tilemapEnd3.height
        );

        this.nextzone4 = new Phaser.Rectangle(
            this.tilemapEnd4.x,
            this.tilemapEnd4.y,
            this.tilemapEnd4.width,
            this.tilemapEnd4.height
        );
    },
    _playerHurt: function(bullet, player) {
        // enemy.wraper.hurt(bullet.wraper.damage);
        // bullet.kill();
    },
    _initEnemy: function() {
        var enemyPosition = [
            { x: 4443, y: 1607 },
            { x: 5094, y: 1757 },
            { x: 5157, y: 1137 },
            { x: 5094, y: 1757 },
            { x: 5157, y: 1137 },
            { x: 5196, y: 487 },
            { x: 4582, y: 3156 },
            { x: 4582, y: 3716 },
            { x: 5152, y: 3716 },
            { x: 5152, y: 3206 },
            { x: 5222, y: 3396 },
            { x: 2370, y: 3813 },
            { x: 1640, y: 3813 },
            { x: 2010, y: 3163 },
            { x: 2330, y: 2623 },
            { x: 1690, y: 2570 }
        ];

        var enemySample = new BossObj(
            "enemy5",
            150,
            1,
            "weapon13",
            "bullet2",
            game,
            {
                x: 722,
                y: 530
            },
            this.enemyGroup,
            this.enemyBulletGroup,
            this.player.character
        );

        for (var i = 0; i < enemyPosition.length; i++) {
            var enemySample = new BigSkeleton(
                "enemy1ss",
                10,
                1,
                "weapon13",
                "bullet2",
                game,
                {
                    x: enemyPosition[i].x,
                    y: enemyPosition[i].y
                },
                this.enemyGroup,
                this.enemySmashGroup,
                this.player.character
            );
            var enemySample = new SkeletonBow(
                "enemy3ss",
                8,
                1,
                "enemyArrow",
                "bullet2",
                game,
                {
                    x: enemyPosition[i].x,
                    y: enemyPosition[i].y
                },
                this.enemyGroup,
                this.enemyBulletGroup,
                this.player.character
            );
        }
    }
};
