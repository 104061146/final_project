var hallState = {
    create: function() {
        console.log("[hallState]: load assets for playState");

        character1 = game.add.button(
            game.width / 4,
            game.height / 4,
            "character1",
            this.start,
            this
        );
        character1.anchor.setTo(0.5, 0.5);
        character1.input.useHandCursor = true;

        character2 = game.add.button(
            (game.width * 3) / 4,
            (game.height * 3) / 4,
            "character2",
            this.start,
            this
        );
        character2.anchor.setTo(0.5, 0.5);
        character2.input.useHandCursor = true;

        character3 = game.add.button(
            game.width / 4,
            (game.height * 3) / 4,
            "character3",
            this.start,
            this
        );
        character3.anchor.setTo(0.5, 0.5);
        character3.input.useHandCursor = true;

        character4 = game.add.button(
            (game.width * 3) / 4,
            game.height / 4,
            "character4",
            this.start,
            this
        );
        character4.anchor.setTo(0.5, 0.5);
        character4.input.useHandCursor = true;
    },
    start: function(e) {
		// console.log(e)
		game.global.player=e.key;
        game.state.start("play");
    },

    characterCallback: function() {
        console.log("callback");
    }
};
