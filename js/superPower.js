// this is a template for sp

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if (new Date().getTime() - start > milliseconds) {
            break;
        }
    }
}

class Base {
    constructor(name, game) {
        this.name = name;
        this.game = game;
        //this.attribute()
        //this.GUI();
    }
    attribute() {
        this.speed = 600;
        this.fullhealth = 2;
        this.health = 2;
        this.fullmp = 200;
        this.mp = 200;
        this.coldTime = 30;
        this.durationTime = 10;
    }
    create() {}
    movePlayer() {}
    pickUpWeapon(player, Box) {
        console.log("over", Box.pick);
        if (Box.open) {
            Box.animations.play("openBox");
            Box.open = false;
            Box.wraper.weapon.arms.visible = true;

        }

        if (Box.pick) {
            console.log("PICK", this);
            console.log("test", this.weapon);
            //console.log("Box", Box.weapon)
            //console.log("BB",Box.test)
            var weapon = Box.wraper.weapon;
            var temp = this.weapon;
            console.log(Box.wraper.weapon.arms);
            /// before the weapon.arms be killed, store the arms imformation
            var scale = Box.wraper.weapon.arms.scale; /// before the weapon.arms be killed, store the arms imformation

            var anchor = Box.wraper.weapon.arms.anchor;
            var test = Box.wraper.scale;
            scale.x *= test;
            scale.y *= test;

            var _scale = temp.arms.scale;
            _scale.x /= test;
            _scale.y /= test;
            var _anchor = temp.arms.anchor;
            ///
            Box.wraper.weapon.arms.kill(); // kill Box's children
            this.weapon.arms.kill();
            this.weapon = weapon; //change the weapon
            Box.wraper.weapon = temp; // 把原本的武器丟地上
            Box.wraper.weapon.arms = Box.addChild(
                game.make.sprite(
                    temp.arms.position.x,
                    temp.arms.position.y,
                    temp.arms.key
                )
            );
            Box.wraper.weapon.arms.scale = _scale;
            Box.wraper.weapon.arms.anchor = _anchor;

            this.weapon.arms = this.character.addChild(
                game.make.sprite(
                    weapon.arms.position.x,
                    weapon.arms.position.y,
                    weapon.arms.key
                )
            );
            this.weapon.arms.scale = scale;
            //this.weapon.arms.scale.setTo(test);
            this.weapon.arms.anchor = anchor;
            this.weapon.enemy = Box.wraper.weapon.enemy;
            this.weapon.player = Box.wraper.weapon.player;

            if (this.weapon.explo) {
                /// 將原本動畫資訊store
                var scale = this.weapon.explo.scale;
                var anchor = this.weapon.explo.anchor;
                ///

                this.weapon.explo = this.character.addChild(
                    game.make.sprite(-25, -0, this.weapon.explo.key)
                );
                this.weapon.explo.scale = scale;
                this.weapon.explo.anchor = anchor;

                if (this.weapon.explo.key == "cut") {
                    this.weapon.explo.animations.add(
                        "cut",
                        [0, 1, 2, 3],
                        24,
                        false
                    );
                } else if (this.weapon.explo.key == "flame") {
                    this.weapon.explo.animations.add(
                        "flame",
                        [0, 1, 2],
                        8,
                        false
                    );
                } else if (this.weapon.explo.key == "stick") {
                    this.weapon.explo.animations.add(
                        "cut",
                        [0, 1, 2, 3, 4, 5, 6, 7, 8],
                        48,
                        false
                    );
                    this.weapon.explo.scale = scale;
                }

                this.weapon.explo.kill();
            }

            // console.log (weapon)
            Box.pick = false;

            //this.weapon.explo=Box.weapon.explo;
            //Box.addChild(temp);

            // console.log(player,"player");
            // console.log(Box,"Box");
        }
    }
    GUI() {
        this.scoreLabel = game.add.text(
            500,
            15,
            "Score: " + game.global.score, {
                font: "18px Geo",
                fill: "#ffffff"
            }
        );
        this.hpLabel = game.add.text(30, 15, "Life", {
            font: "18px Geo",
            fill: "#ffffff"
        });

        this.mpLabel = game.add.text(30, 55, "Power", {
            font: "18px Geo",
            fill: "#ffffff"
        });
        this.volumeLabel = game.add.text(300, 15, "Volume", {
            font: "18px Geo",
            fill: "#ffffff"
        });

        console.log(this.health);
        this.HP = game.add.text(250, 30, this.health.toString(), {
            font: "24px Geo",
            fill: "#ffffff"
        });
        this.MP = game.add.text(250, 60, this.mp.toString(), {
            font: "24px Geo",
            fill: "#ffffff"
        });

        this.lifeBar = this.game.add.sprite(30, 40, "progressBar");
        this.lifeBar.anchor.setTo(0, 0);
        this.lifeBar.scale.setTo(0.5 * (this.health / this.fullhealth), 0.3);
        this.powerBar = this.game.add.sprite(30, 80, "progressBar");
        this.powerBar.anchor.setTo(0, 0);
        this.powerBar.scale.setTo(0.5 * (this.mp / this.fullmp), 0.3);
        this.lifeVal = 1.0;
        this.powerVal = 1.0;
        this.scoreLabel.fixedToCamera = true;
        this.hpLabel.fixedToCamera = true;
        this.mpLabel.fixedToCamera = true;
        this.volumeLabel.fixedToCamera = true;
        this.lifeBar.fixedToCamera = true;
        this.powerBar.fixedToCamera = true;
        this.HP.fixedToCamera = true;
        this.MP.fixedToCamera = true;


        this.scoreLabel.cameraOffset.setTo(500, 15);
        this.hpLabel.cameraOffset.setTo(30, 15);
        this.mpLabel.cameraOffset.setTo(30, 55);
        this.volumeLabel.cameraOffset.setTo(300, 15);
        this.lifeBar.cameraOffset.setTo(30, 40);
        this.powerBar.cameraOffset.setTo(30, 80);
        this.HP.cameraOffset.setTo(250, 30);
        this.MP.cameraOffset.setTo(250, 70);


    }
    changeLife(val) {
        if (this.health + val < 0) {
            this.playerDie();
            return false;
        }
        this.health += val;
        console.log("change life", this.health, this.lifeBar);
        this.lifeBar.scale.setTo(0.5 * (this.health / this.fullhealth), 0.3);
        this.HP.setText(this.health.toString());

        return true;
    }
    playerHurt(player, bullet) {
        // console.log("enter!!!");
        // console.log(this);
        this.changeLife(-bullet.atk);
        bullet.kill();
    }
    changePower(val) {
        if (this.mp + val < 0) {
            return false;
        }
        this.mp += val;
        console.log(this.mp);
        console.log(this.mp / this.fullmp);
        console.log(this.powerBar.scale);
        this.powerBar.scale.setTo(0.5 * (this.mp / this.fullmp), 0.3);

        this.MP.setText(this.mp.toString());


        // this.powerBar.scale.x = 0.5 * (this.mp/this.fullmp)
        // sleep(1000);
        return true;
    }
    playerDie() {
        var person = prompt("Game Over!! Please enter your name", "player");
        if (person != null) {
            // firebase.database().ref('leader_board').push({
            //     name: person,
            //     scores: game.global.score,
            // });
            console.log("you lose!");
        }
        var r = confirm("Play again?");
        if (r) {
            game.state.start("play");
        } else {
            game.state.start("menu");
        }
    }
}

class Box extends Base {
    constructor(name, game, enemyGroup, x, y) {
        super(name, game);

        this.character = this.game.add.sprite(x, y, this.name);
        this.character.pick = false;
        this.time = 0;
        this.enemyGroup = enemyGroup;
        this.scale = 3;
        this.character.scale.setTo(this.scale);
        this.character.animations.add(
            "openBox",
            [0, 1, 2, 3, 4, 5, 6, 7],
            5,
            false
        );
        this.character.wraper = this;
        this.character.open = true;

        //this.character.height = 50;
        //this.character.width = 50;
        this.character.anchor.setTo(0.5, 0.5);
        //this.character.visible=false; 
        var random = this.game.rnd.integerInRange(0, 10);
        if (random == 1) {
            this.weapon = new Snow("hi", game, this.character, this.enemyGroup);

        } else if (random == 2) {
            this.weapon = new Needle("hi", game, this.character, this.enemyGroup);
        } else if (random == 3) {
            this.weapon = new Sword("hi", game, this.character, this.enemyGroup);
        } else if (random == 4) {
            this.weapon = new GoldSword("hi", game, this.character, this.enemyGroup);
        } else if (random == 5) {
            this.weapon = new FireSword("hi", game, this.character, this.enemyGroup);
        } else if (random == 6) {
            this.weapon = new Flame("hi", game, this.character, this.enemyGroup);
        } else if (random == 7) {
            this.weapon = new Needle("hi", game, this.character, this.enemyGroup);
        } else if (random == 8) {
            this.weapon = new IcePike("hi", game, this.character, this.enemyGroup);
        } else if (random == 9) {
            this.weapon = new Bow("hi", game, this.character, this.enemyGroup);
        } else if (random == 0) {
            this.weapon = new Wand("hi", game, this.character, this.enemyGroup);
        } else if (random == 10) {
            this.weapon = new Axe("hi", game, this.character, this.enemyGroup);
        }
        //this.weapon = new GoldSword("EEE", this.game, this.character, enemyGroup);
        //console.log("this",this.weapon)
        //this.weapon.arms.visible=false;
        this.weapon.arms.scale.x /= this.scale;
        this.weapon.arms.scale.y /= this.scale;
        this.weapon.arms.visible = false;


        //this.weapon.arms.scale.setTo(1 / this.scale);
        //console.log("this",this.weapon)

        //this.character.weapon = this.weapon;
        this.game.physics.arcade.enable(this.character);
        this.X = this.game.input.keyboard.addKey(Phaser.Keyboard.X);
        // this.lifeBar.kill();
        // this.powerBar.kill();

        // console.log('box', this.powerBar);
    }
    movePlayer() {
        if (this.X.isDown) {
            //console.log(this.game.this.time)
            if (this.game.time.now > this.time) {
                this.character.pick = true;
                this.time = this.game.time.now + 1000;
            }
        } //if(this.X.isUP)
        else {
            this.character.pick = false;
        }
    }
}

class CharacterJon extends Base {
    constructor(
        game,
        name = "character3",
        effect = "sp_shield",
        enemyGroup = undefined
    ) {
        super(name, game);
        this.effect_name = effect;
        this.enemyGroup = new Array();
        this.attribute();
        this.create();
        this.GUI();
    }
    create() {
        this.cold_bool = 1;
        this.check_fire = 0;
        this.sheldflage = true;

        this.character = this.game.add.sprite(
            this.game.width / 2,
            this.game.height / 2,
            this.name
        );
        this.character.anchor.setTo(0.5, 0.5);
        this.character.scale.setTo(1, 1);

        this.character.speed = this.speed;
        this.character.health = this.health;


        this.weapon = new GoldSword("hi", game, this.character, this.enemyGroup);

        //this.character.weapon = new IcePike("hi", game, this.character, this.enemyGroup);
        // this.weapon = new Flame("hi", game, this.character, this.enemyGroup);
        // this.weapon = new Sword("hi", game, this.character, this.enemyGroup);
        // this.weapon = new IcePike("hi", game, this.character, this.enemyGroup);
        // this.weapon = new Bow("hi", game, this.character);

        this.effect = this.game.add.sprite(
            this.character.x,
            this.character.y,
            this.effect_name
        );

        this.effect.anchor.setTo(0.5, 0.5);
        this.effect.scale.setTo(0, 0);

        this.effect2 = this.game.add.sprite(
            this.character.x,
            this.character.y,
            "sp_shield"
        );
        this.effect2.anchor.setTo(0.5, 0.5);
        this.effect2.scale.setTo(0, 0);

        this.game.physics.arcade.enable(this.effect);
        this.game.physics.arcade.enable(this.effect2);
        this.game.physics.arcade.enable(this.character);
        // this.character.body.collideWorldBounds = true;

        this.cursor = this.game.input.keyboard.createCursorKeys();
        this.Z = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    }

    movePlayer() // called in update()
    {
        if (this.cursor.left.isDown) {
            this.character.body.velocity.x = -this.character.speed;
            this.character.scale.x = -1;
        } else if (this.cursor.right.isDown) {
            this.character.body.velocity.x = this.character.speed;
            this.character.scale.x = 1;
        }

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) {
            this.character.body.velocity.y = -this.character.speed;
        } else if (this.cursor.down.isDown) {
            this.character.body.velocity.y = this.character.speed;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player
            this.character.body.velocity.x = 0;
            this.character.body.velocity.y = 0;
        }

        this.effect.x = this.character.x;
        this.effect.y = this.character.y;
        this.effect2.x = this.character.x;
        this.effect2.y = this.character.y;
    }
    checkSP() // called in update() to check whether use the skill
    {
        if (this.Z.isDown && this.cold_bool) {
            this.check_fire = 1;
            this.effect.scale.setTo(0.25, 0.25);
            this.effect2.scale.setTo(0.5, 0.5);
            this.game.time.events.add(
                Phaser.Timer.SECOND * this.durationTime,
                this.hide,
                this
            );
        }
    }
    hide() {
        this.check_fire = 0;
        this.effect.scale.setTo(0, 0);
        this.effect2.scale.setTo(0, 0);
        this.cold_bool = 0;
        this.game.time.events.add(
            Phaser.Timer.SECOND * this.coldTime,
            this.coldTimeFn,
            this
        );
    }
    deffense(bullet, sheld) {
        console.log("testtt", bullet, sheld);
        sheld.kill();
    };
    coldTimeFn() {
        this.cold_bool = 1;
    };


}

class enemy extends Base {
    constructor(name, game) {
        super(name, game);
    }
}

class CharacterDaenerys extends Base {
    constructor(
        game,
        name = "character2",
        effect = "fire_ball",
        enemyGroup = undefined
    ) {
        super(name, game);
        this.enemyGroup = new Array();
        this.effect_name = effect;
        this.attribute();
        this.create();
        this.GUI();
        // this.powerBar.visible = false;
        console.log('character', this.powerBar);
    }
    attribute() {
        this.speed = 600;
        this.fullhealth = 10;
        this.health = 10;
        this.fullmp = 250;
        this.mp = 250;
        this.durationTime = 10;
        this.coldTime = 30;
    }
    create() {
        this.character_dragon = this.game.add.sprite(
            this.game.width / 2,
            this.game.height / 2,
            "dragon"
        );
        this.character_dragon.anchor.setTo(0.5, 0.5);
        this.character_dragon.scale.setTo(0, 0);
        this.character_dragon.animations.add("upwards", [0, 1, 2], 5, true);
        this.character_dragon.animations.add("leftward", [9, 10, 11], 5, true);
        this.character_dragon.animations.add("downward", [6, 7, 8], 5, true);
        this.character_dragon.animations.add("rightward", [3, 4, 5], 5, true);

        this.character = this.game.add.sprite(
            this.game.width / 2,
            this.game.height / 2,
            this.name
        );
        this.character.anchor.setTo(0.5, 0.5);
        this.character.scale.setTo(1, 1);
        this.character.health = this.health;

        this.game.physics.arcade.enable(this.character);
        this.character.body.collideWorldBounds = true;
        this.character.speed = this.speed;
        //  Creates 30 bullets, using the 'bullet' graphic
        // this.weapon= new Wand("hi", game, this.character, this.enemyGroup);
        this.weapon = new GoldSword(
            "hi",
            game,
            this.character,
            this.enemyGroup
        );
        // this.weapon = new Flame("hi", game, this.character, this.enemyGroup);
        this.fireball = this.game.add.weapon(100, this.effect_name);
        this.fireball.damage = 1;
        this.fireball.bullets.wraper = this.fireball;
        // console.log(this.fireball.bullets.wraper);

        this.game.physics.arcade.enable(this.fireball);

        this.fireball.setBulletFrames(0, 0, true);
        this.fireball.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;

        //  The speed at which the bullet is fired
        this.fireball.bulletSpeed = 1000;

        //  Speed-up the rate of fire, allowing them to shoot 1 bullet every 50ms
        this.fireball.fireRate = 1;
        this.fireball.trackSprite(this.character, 0, 0, false);

        this.cursor = this.game.input.keyboard.createCursorKeys();
        this.Z = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.fireButton = this.game.input.keyboard.addKey(
            Phaser.KeyCode.SPACEBAR
        );
        this.cold_bool = 1;
    }
    movePlayer() {
        if (this.cursor.left.isDown) {
            if (this.character.scale.x > 0)
                this.character.scale.x = -this.character.scale.x;
            this.character.body.velocity.x = -this.character.speed;
            this.character_dragon.play("leftward");
            this.character_dragon.x = this.character.x;
            this.character_dragon.y = this.character.y + 30;
            this.fireball.fireAngle = 180;
        } else if (this.cursor.right.isDown) {
            if (this.character.scale.x < 0)
                this.character.scale.x = -this.character.scale.x;
            this.character.body.velocity.x = this.character.speed;
            this.character_dragon.play("rightward");
            this.character_dragon.x = this.character.x;
            this.character_dragon.y = this.character.y + 30;
            this.fireball.fireAngle = 0;
        }

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) {
            this.character.body.velocity.y = -this.character.speed;
            this.fireball.fireAngle = -90;
            this.character_dragon.x = this.character.x;
            this.character_dragon.y = this.character.y + 30;
            // console.log(this.weapon);
            this.character_dragon.play("upwards");
        } else if (this.cursor.down.isDown) {
            this.character.body.velocity.y = this.character.speed;
            this.fireball.fireAngle = 90;
            this.character_dragon.x = this.character.x;
            this.character_dragon.y = this.character.y + 30;
            // console.log(this.weapon);
            this.character_dragon.play("downward");
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player
            this.character.body.velocity.x = 0;
            this.character.body.velocity.y = 0;
        }
    }
    checkSP() {
        if (this.Z.isDown && this.cold_bool) {
            this.check_fire = 1;
            this.character.scale.setTo(0.5, 0.5);
            this.character_dragon.scale.setTo(1.5, 1.5);

        }
    }
    fireFn() {
        if (this.fireButton.isDown && this.check_fire) {
            console.log(this.fireball.weapon);
            this.fireball.fire();
            this.game.time.events.add(
                Phaser.Timer.SECOND * this.durationTime,
                this.hide,
                this
            );
        }
    }
    hide() {
        this.cold_bool = 0;
        this.check_fire = 0;
        this.character.scale.setTo(1, 1);
        this.character_dragon.scale.setTo(0, 0);
        this.game.time.events.add(
            Phaser.Timer.SECOND * this.coldTime,
            this.coldTimeFn,
            this
        );
    }
    coldTimeFn() {
        this.cold_bool = 1;
    }
}

class CharacterArya extends Base {
    constructor(
        game,
        name = "character4",
        effect = "speed",
        enemyGroup = undefined
    ) {
        super(name, game);
        this.enemyGroup = new Array();
        this.effect_name = effect;
        this.attribute();
        this.create();
        this.GUI();
    }
    attribute() {
        this.speed = 600;
        this.fullhealth = 10;
        this.health = 10;
        this.fullmp = 300;
        this.mp = 300;
        this.coldTime = 30;
        this.durationTime = 10;
    }
    create() {
        this.effect = this.game.add.sprite(
            this.game.width / 2,
            this.game.height / 2,
            this.effect_name
        );
        this.effect.anchor.setTo(0.5, 0.5);
        this.effect.scale.setTo(0, 0);
        this.effect.speed = 2000;
        this.effect.animations.add("rotate", [0, 1, 2, 3], 10, true);

        this.character = this.game.add.sprite(
            this.game.width / 2,
            this.game.height / 2,
            this.name
        );

        this.weapon = new Needle("hi", game, this.character, this.enemyGroup);

        this.character.anchor.setTo(0.5, 0.5);
        this.character.scale.setTo(1, 1);

        this.game.physics.arcade.enable(this.character);

        this.character.health = this.health;

        this.cursor = this.game.input.keyboard.createCursorKeys();
        console.log(this.cursor);
        this.Z = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.cold_bool = 1;
    }
    movePlayer() {
        if (this.cursor.left.isDown) {
            this.character.body.velocity.x = -this.speed;
        } else if (this.cursor.right.isDown) {
            this.character.body.velocity.x = this.speed;
        }

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) {
            this.character.body.velocity.y = -this.speed;
        } else if (this.cursor.down.isDown) {
            this.character.body.velocity.y = this.speed;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player
            this.character.body.velocity.x = 0;
            this.character.body.velocity.y = 0;
            this.effect.x = this.character.x + 10;
            this.effect.y = this.character.y + 40;
        }
        // this.effect2.x = this.character.x;
        // this.effect2.y = this.character.y;
    }
    checkSP() {
        if (this.Z.isDown && this.cold_bool) {
            this.cold_bool = 0;
            this.speed = 950;
            this.effect.animations.play("rotate");
            this.effect.scale.setTo(0.2, 0.2);
            this.game.time.events.add(
                Phaser.Timer.SECOND * this.durationTime,
                this.hide,
                this
            );
        }
    }
    hide() {
        this.effect.scale.setTo(0, 0);

        // this.effect2.scale.setTo(0,0);

        this.character.speed = 600;
        this.game.time.events.add(
            Phaser.Timer.SECOND * this.coldTime,
            this.coldTimeFn,
            this
        );
    }
    coldTimeFn() {
        this.cold_bool = 1;
    }
}

class CharacterMelisandre extends Base {
    constructor(
        game,
        name = "character1",
        effect = "WitchEffect",
        enemyGroup = undefined
    ) {
        super(name, game);
        this.enemyGroup = new Array();
        this.effect_name = effect;
        console.log("enter character");
        this.attribute();
        this.create();
        this.GUI();
    }
    attribute() {
        this.speed = 600;
        this.fullhealth = 5;
        this.health = 5;
        this.fullmp = 150;
        this.mp = 150;
        this.coldTime = 30;
        this.durationTime = 10;
    }
    create() {
        this.character = this.game.add.sprite(
            this.game.width / 2,
            this.game.height / 2,
            this.name
        );

        this.effect = this.game.add.sprite(
            this.game.width / 2,
            this.game.height / 2,
            this.effect_name
        );

        this.weapon = new Wand("hi", game, this.character, this.enemyGroup);

        this.character.anchor.setTo(0.5, 0.5);
        this.character.scale.setTo(1, 1);
        this.character.speed = this.speed;
        this.character.health = this.health;

        this.effect.anchor.setTo(0.5, 0.5);
        this.effect.scale.setTo(0, 0);
        this.effect.speed = 2000;
        this.effect.animations.add(
            "rotate",
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
            14,
            true
        );

        this.game.physics.arcade.enable(this.character);
        // this.character.body.collideWorldBounds = true;

        this.cursor = this.game.input.keyboard.createCursorKeys();
        console.log(this.cursor);
        this.Z = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.cold_bool = 1;
    }
    movePlayer() {
        if (this.cursor.left.isDown) {
            this.character.body.velocity.x = -this.character.speed;
        } else if (this.cursor.right.isDown) {
            this.character.body.velocity.x = this.character.speed;
        }

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) {
            this.character.body.velocity.y = -this.character.speed;
        } else if (this.cursor.down.isDown) {
            this.character.body.velocity.y = this.character.speed;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player
            this.character.body.velocity.x = 0;
            this.character.body.velocity.y = 0;
        }
        this.effect.x = this.character.x + 5;
        this.effect.y = this.character.y + 40;
        // this.effect2.x = this.character.x;
        // this.effect2.y = this.character.y;
    }
    checkSP() {
        if (this.Z.isDown && this.cold_bool) {
            this.effect.animations.play("rotate");
            this.effect.scale.setTo(1, 1);
            var i;
            for (i = 0; i < 5; i++) {
                if (this.character.health <= 95) {
                    console.log(this.character.health);
                    this.game.time.events.add(
                        Phaser.Timer.SECOND * (i+1),
                        this.addHealthFn,
                        this
                    );
                    if (this.health < this.fullhealth) {
                        this.changeLife(1);
                    }
                }
            }
            this.game.time.events.add(
                Phaser.Timer.SECOND * this.durationTime,
                this.hide,
                this
            );
        }
    }

    hide() {
        this.effect.scale.setTo(0, 0);
        this.cold_bool = 0;
        // this.character.speed -= 100;
        this.game.time.events.add(
            Phaser.Timer.SECOND * this.coldTime,
            this.coldTimeFn,
            this
        );
    }
    coldTimeFn() {
        this.cold_bool = 1;
    }
    addHealthFn() {
        this.character.health += 1;
    }
    attackFn() {}
}