class Weapons {
    constructor(type, game, player, enemy) {
        this.type = type || null;
        this.game = game || null;
        this.player = player || null;
        this.damage = null;
        this.frequency = null;
        this.accuration = null;
        this.energy = null;
        this.enemy = enemy || null;
    }
    getenemy(x, y, distance, enemies) {
        var enemyUnits = [];
        enemies.forEachAlive(child => {
            enemyUnits.push(child);
        });
        for (var i = 0; i < enemyUnits.length; i++) {
            if (
                Phaser.Math.distance(x, y, enemyUnits[i].x, enemyUnits[i].y) <=
                distance
            ) {
                return enemyUnits[i];
            }

        }
        return false;
    }
    collision(bullet, enemy) {
        bullet.kill();
        enemy.kill();
    }
}

///   Snow sword
class Snow extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 2;
        this.arms = this.player.addChild(game.make.sprite(-25, -0, "weapon9"));
        this.arms.anchor;
        this.arms.scale.setTo(1, 1);
        this.pixel = this.game.add.sprite(100, 100, "pixel");
        this.pixel.width = 150;
        this.pixel.height = 150;
        this.pixel.visible = false;
        this.pixel.kill();
        this.game.physics.enable(this.pixel, Phaser.Physics.ARCADE);
        this.game.physics.enable(this.arms, Phaser.Physics.ARCADE);
        this.arms.enableBody = true;
        this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletTime = 0;
        //this.explo = this.game.add.sprite(this.player.x, this.player.y, 'cut');
        this.explo = this.player.addChild(game.make.sprite(-25, -0, "cut"));
        this.explo.anchor.setTo(0.5, 0.5);
        this.explo.scale.setTo(0.5, 0.5);
        //this.explo.animations.add('cut',[7,6,5,4,3,2,1,0,8],24,false);
        this.explo.animations.add("cut", [0, 1, 2, 3], 24, false);
        this.explo.kill();
        this.bullet = this.pixel;
        this.bullet.wraper = this;
    }
    attack() {
        if (this.game.time.now > this.bulletTime) {
            // if (this.player.scale.x == 1) {
            //     this.pixel.reset(this.player.x + 50, this.player.y - 100);
            // } else {
            //     this.pixel.reset(this.player.x - 150, this.player.y - 100);
            // }
            // this.pixel.visible = false;
            console.log("Snow",)
            this.bulletTime = this.game.time.now + 300;
            this.game.add
                .tween(this.arms)
                .to({
                        angle: -140
                    },
                    50
                )
                .start();
            var set = function (explo, game, sword, pixel, player) {
                if (player.scale.x == 1) {
                    pixel.reset(player.x - 20, player.y - 80);
                } else {
                    pixel.reset(player.x - 130, player.y - 80);
                }
                pixel.visible = false;
                explo.reset(20, 0);
                game.add
                    .tween(sword)
                    .to({
                            angle: 120
                        },
                        100
                    )
                    .to({
                            angle: 0
                        },
                        50
                    )
                    .start();
                explo.animations.play("cut");
                explo.animations.currentAnim.onComplete.add(function () {
                    explo.kill();
                }, explo);
            };
            setTimeout(
                set.bind(
                    "undefined",
                    this.explo,
                    this.game,
                    this.arms,
                    this.pixel,
                    this.player
                ),
                200
            );
        }
    }
}
/// Fire sword
class FireSword extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 2;
        this.arms = this.player.addChild(game.make.sprite(-25, -0, "weapon12"));
        this.arms.anchor;
        this.arms.scale.setTo(1, 1);
        this.game.physics.enable(this.arms, Phaser.Physics.ARCADE);
        this.arms.enableBody = true;
        this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletTime = 0;
        ///
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(1000, "bullet3");
        this.bullets.setAll("anchor.x", 0);
        this.bullets.setAll("anchor.y", 0.5);
        this.bullets.setAll("outOfBoundsKill", true);
        this.bullets.setAll("checkWorldBounds", true);
    }
    attack() {
        if (this.game.time.now > this.bulletTime) {

            this.bulletTime = this.game.time.now + 1000;
            this.game.add
                .tween(this.arms)
                .to({
                        angle: -140
                    },
                    300
                )
                .start();
            var set = function (explo, game, sword, pixel, player) {

                game.add
                    .tween(sword)
                    .to({
                            angle: 120
                        },
                        100
                    )
                    .to({
                            angle: 0
                        },
                        50
                    )
                    .start();

            };
            setTimeout(
                set.bind(
                    "undefined",
                    this.explo,
                    this.game,
                    this.arms,
                    this.pixel,
                    this.player
                ),
                300
            );
            this.bullet = this.bullets.getFirstExists(false);

            if (this.bullet) {
                this.bullet.wraper = this;
                //  And fire it
                this.bullet.reset(this.player.x, this.player.y + 8);
                this.bullet.scale.setTo(0.25, 0.25);
                this.bullet.lifespan = 3000;
                //get enemy
                var vec = 1000;
                var enemy = this.getenemy(
                    this.player.x,
                    this.player.y,
                    500,
                    this.enemy[0]
                );
                //enemy=null;

                if (enemy) {
                    //calculate angle between enemy and player;
                    // if (this.player.scale.x == 1) {
                    //     this.bullet.scale.x=-1;
                    // }
                    // if (this.player.scale.x == -1) {
                    //     this.bullet.scale.x=1;
                    // }
                    var angle = game.math.angleBetween(
                        this.player.x,
                        this.player.y,
                        enemy.x,
                        enemy.y
                    );
                    this.bullet.body.velocity.y = vec * Math.sin(angle);
                    this.bullet.body.velocity.x = vec * Math.cos(angle);
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    this.bullet.angle = (angle / Math.PI) * 180;

                } else {
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    if (this.player.scale.x == 1) {
                        this.bullet.body.velocity.x = vec;
                        this.bullet.angle=0;

                    }
                    if (this.player.scale.x == -1) {
                        this.bullet.body.velocity.x = -vec;
                        this.bullet.angle=180;
                    }
                }
            }
        }
    }
}
/// Gold sword
class GoldSword extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 10;
        this.arms = this.player.addChild(game.make.sprite(0, -10, "weapon11"));
        this.arms.anchor.setTo(0, 0.5);
        this.arms.scale.setTo(0.25, 0.25);
        //this.arms.angle=45;
        this.pixel = this.game.add.sprite(100, 100, "pixel");
        this.pixel.width = 100;
        this.pixel.height = 100;
        this.pixel.visible = false;
        this.pixel.kill();
        this.game.physics.enable(this.pixel, Phaser.Physics.ARCADE);
        this.game.physics.enable(this.arms, Phaser.Physics.ARCADE);
        this.arms.enableBody = true;
        this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletTime = 0;
        this.explo = this.player.addChild(game.make.sprite(-25, -0, "cut"));
        this.explo.anchor.setTo(0.5, 0.5);
        this.explo.scale.setTo(0.5, 0.5);
        this.explo.animations.add("cut", [0, 1, 2, 3], 24, false);
        this.explo.kill();
        this.bullet = this.pixel;
        this.bullet.wraper = this;
    }
    attack() {
        if (this.game.time.now > this.bulletTime) {
            this.bulletTime = this.game.time.now + 300;
            this.game.add
                .tween(this.arms)
                .to({
                        angle: 0,
                    },
                    300
                )
                .start();
            var set = function (explo, game, sword, pixel, player) {
                if (player.scale.x == 1) {
                    pixel.reset(player.x - 20, player.y - 80);
                } else {
                    pixel.reset(player.x - 130, player.y - 80);
                }
                pixel.visible = false;
                explo.reset(20, 0);
                game.add
                    .tween(sword)
                    .to({
                            angle: 120
                        },
                        100
                    )
                    .to({
                            angle: 45
                        },
                        50
                    )
                    .start();
                explo.animations.play("cut");
                explo.animations.currentAnim.onComplete.add(function () {
                    explo.kill();
                }, explo);
            };
            setTimeout(
                set.bind(
                    "undefined",
                    this.explo,
                    this.game,
                    this.arms,
                    this.pixel,
                    this.player
                ),
                50
            );
        }
    }
}


/// Ice sword 

class Sword extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 0;
        this.arms = this.player.addChild(game.make.sprite(-25, -0, "weapon2"));
        this.arms.anchor;
        this.arms.scale.setTo(1.5, 1.5);
        this.pixel = this.game.add.sprite(100, 100, "pixel");
        this.pixel.width = 100;
        this.pixel.height = 100;
        this.pixel.visible = false;
        this.pixel.kill();
        this.game.physics.enable(this.pixel, Phaser.Physics.ARCADE);
        this.game.physics.enable(this.arms, Phaser.Physics.ARCADE);
        this.arms.enableBody = true;
        this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletTime = 0;
        //this.explo = this.game.add.sprite(this.player.x, this.player.y, 'cut');
        
        this.bullet = this.pixel;
        this.bullet.wraper = this;
    }
    attack() {
        if (this.game.time.now > this.bulletTime) {
            this.bulletTime = this.game.time.now + 1000;
            this.game.add
                .tween(this.arms)
                .to({
                        angle: -140
                    },
                    300
                )
                .start();
            var set = function (explo, game, sword, pixel, player) {
                if (player.scale.x == 1) {
                    pixel.reset(player.x - 20, player.y - 80);
                } else {
                    pixel.reset(player.x - 130, player.y - 80);
                }
                pixel.visible = false;
                game.add
                    .tween(sword)
                    .to({
                            angle: 120
                        },
                        100
                    )
                    .to({
                            angle: 0
                        },
                        50
                    )
                    .start();
                
            };
            setTimeout(
                set.bind(
                    "undefined",
                    this.explo,
                    this.game,
                    this.arms,
                    this.pixel,
                    this.player
                ),
                500
            );
        }
    }
}

///  Wand
class Wand extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 2;
        //
        this.arms = this.player.addChild(game.make.sprite(8, 16, "weapon5"));
        this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        /// wand bullet
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(1000, "bullet4");
        this.bullets.setAll("anchor.x", 0.5);
        this.bullets.setAll("anchor.y", 1);
        this.bullets.setAll("outOfBoundsKill", true);
        this.bullets.setAll("checkWorldBounds", true);
        this.bulletTime = 0;
        /// wand's position
        /// wand particle
        this.explosions = this.game.add.group();
        this.explosions.createMultiple(300, "pixel");
        this.explosions.forEach(function (invader) {
            invader.anchor.x = 0.5;
            invader.anchor.y = 0.5;
            invader.scale.setTo(0.5, 0.5);
            invader.animations.add("pixel");
        }, this);
    }
    attack() {
        if (this.game.time.now > this.bulletTime) {
            //  Grab the first bullet we can from the pool
            this.bullet = this.bullets.getFirstExists(false);

            if (this.bullet) {
                this.bullet.wraper = this;
                //  And fire it
                this.bullet.reset(this.player.x, this.player.y + 8);
                this.bullet.scale.setTo(0.25, 0.25);
                this.bullet.lifespan = 3000;
                //get enemy
                var vec = 1000;
                var enemy = this.getenemy(
                    this.player.x,
                    this.player.y,
                    500,
                    this.enemy[0]
                );
                this.game.add
                    .tween(this.arms)
                    .to({
                            angle: -90
                        },
                        50
                    )
                    .yoyo(true, 100)
                    .start();
                if (enemy) {
                    //calculate angle between enemy and player;
                    var angle = game.math.angleBetween(
                        this.player.x,
                        this.player.y,
                        enemy.x,
                        enemy.y
                    );
                    this.bullet.angle = (angle / Math.PI) * 180;

                    this.bullet.body.velocity.y = vec * Math.sin(angle);
                    this.bullet.body.velocity.x = vec * Math.cos(angle);
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                } else {
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    if (this.player.scale.x == 1) {
                        this.bullet.body.velocity.x = vec;
                        this.bullet.angle = 0;

                    }
                    if (this.player.scale.x == -1) {
                        this.bullet.body.velocity.x = -vec;
                        this.bullet.angle = 180;
                    }
                }
            }
        }
    }
}

// Bow
class Bow extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 2;
        //
        this.arms = this.player.addChild(game.make.sprite(8, 16, "weapon1"));
        this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(1000, "bullet5");
        this.bullets.setAll("anchor.x", 0.5);
        this.bullets.setAll("anchor.y", 0.5);
        this.bullets.setAll("outOfBoundsKill", true);
        this.bullets.setAll("checkWorldBounds", true);
        this.bulletTime = 0;
    }
    attack() {
        //console.log("attack",this.we);
        if (this.game.time.now > this.bulletTime) {
            //  Grab the first bullet we can from the pool
            this.bullet = this.bullets.getFirstExists(false);

            if (this.bullet) {
                
                this.bullet.wraper = this;
                //  And fire it
                this.bullet.frame=6;
                this.bullet.scale.setTo(1.5,1.5);
                console.log(this.bullet.frame)
                this.bullet.reset(this.player.x, this.player.y + 8);
                this.bullet.lifespan = 3000;
                //get enemy
                var vec = 1000;
                var enemy = this.getenemy(
                    this.player.x,
                    this.player.y,
                    500,
                    this.enemy[0]
                );
                //enemy=null;
                this.game.add
                    .tween(this.arms)
                    .to({
                            angle: -90
                        },
                        50
                    )
                    .yoyo(true, 100)
                    .start();
                if (enemy) {
                    //calculate angle between enemy and player;
                    var angle = game.math.angleBetween(
                        this.player.x,
                        this.player.y,
                        enemy.x,
                        enemy.y
                    );
                    this.bullet.body.velocity.y = vec * Math.sin(angle);
                    this.bullet.angle = (angle / Math.PI) * 180;
                    this.bullet.body.velocity.x = vec * Math.cos(angle);
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                } else {
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    if (this.player.scale.x == 1) {
                        this.bullet.body.velocity.x = vec;
                        this.bullet.angle = 0;

                    }
                    if (this.player.scale.x == -1) {
                        this.bullet.body.velocity.x = -vec;
                        this.bullet.angle = 180;
                    }
                }
            }
        }
    }
}
/// fire wand

class Flame extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 2;
        ///
        this.arms = this.player.addChild(game.make.sprite(8, 16, "weapon4"));
        this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(1000, "bullet1");
        this.bullets.setAll("anchor.x", 0.5);
        this.bullets.setAll("anchor.y", 0.5);
        this.bullets.setAll("outOfBoundsKill", true);
        this.bullets.setAll("checkWorldBounds", true);
        this.bulletTime = 0;
        this.explo = this.game.add.sprite(0, 0, "flame");
        this.explo.anchor.setTo(0.5, 0.5);
        this.explo.scale.setTo(0.1, 0.1);
        //this.explo.animations.add('cut',[7,6,5,4,3,2,1,0,8],24,false);
        this.explo.animations.add("flame", [0, 1, 2], 8, false);
        this.explo.kill();
    }
    attack() {
        //console.log("attack",this.we);
        if (this.game.time.now > this.bulletTime) {
            //  Grab the first bullet we can from the pool
            this.bullet = this.bullets.getFirstExists(false);

            if (this.bullet) {
                this.bullet.wraper = this;
                //  And fire it
                this.bullet.reset(this.player.x, this.player.y + 8);
                this.bullet.lifespan = 3000;
                this.bullet.scale.setTo(0.05, 0.05);

                //get enemy
                var vec = 1000;
                var enemy = this.getenemy(
                    this.player.x,
                    this.player.y,
                    500,
                    this.enemy[0]
                );
                //enemy=null;
                this.game.add
                    .tween(this.arms)
                    .to({
                            angle: -90
                        },
                        50
                    )
                    .yoyo(true, 100)
                    .start();
                if (enemy) {
                    //calculate angle between enemy and player;
                    var angle = game.math.angleBetween(
                        this.player.x,
                        this.player.y,
                        enemy.x,
                        enemy.y
                    );
                    this.bullet.body.velocity.y = vec * Math.sin(angle);
                    this.bullet.angle = (angle / Math.PI) * 180;
                    this.bullet.body.velocity.x = vec * Math.cos(angle);
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                } else {
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    if (this.player.scale.x == 1) {
                        this.bullet.body.velocity.x = vec;
                        this.bullet.angle = 0;

                    }
                    if (this.player.scale.x == -1) {
                        this.bullet.body.velocity.x = -vec;
                        this.bullet.angle = 180;
                    }
                }
            }
        }
    }
    activateAnimation(bullet, enemy) {
        this.explo.reset(enemy.x, enemy.y);
        this.explo.play("flame");
        this.explo.animations.currentAnim.onComplete.add(function () {
            this.explo.kill();
        }, this);
        // bullet.kill();
        //enemy.kill();
    }
}

/// Axe

class Axe extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 2;
        ///
        this.arms = this.player.addChild(game.make.sprite(8, 16, "weapon6"));
        //this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        this.arms.scale.setTo(0.5, 0.5);
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(100, "weapon6");
        this.bullets.setAll("anchor.x", 0.5);
        this.bullets.setAll("anchor.y", 0.5);
        this.bullets.setAll("outOfBoundsKill", true);
        this.bullets.setAll("checkWorldBounds", true);
        this.bulletTime = 0;

        //
    }
    attack() {
        //console.log("attack",this.we);
        if (this.game.time.now > this.bulletTime) {
            //  Grab the first bullet we can from the pool
            this.bullet = this.bullets.getFirstExists(false);

            if (this.bullet) {
                //  And fire it
                this.bullet.wraper = this;
                this.bullet.reset(this.player.x, this.player.y + 8);
                this.bullet.lifespan = 3000;
                this.bullet.scale.setTo(0.5, 0.5);
                //get enemy
                var vec = 1000;
                var enemy = this.getenemy(
                    this.player.x,
                    this.player.y,
                    500,
                    this.enemy[0]
                );
                //enemy=null;
                this.game.add
                    .tween(this.arms)
                    .to({
                            angle: -90
                        },
                        50
                    )
                    .yoyo(true, 100)
                    .start();
                if (enemy) {
                    //calculate angle between enemy and player;
                    var angle = game.math.angleBetween(
                        this.player.x,
                        this.player.y,
                        enemy.x,
                        enemy.y
                    );
                    this.bullet.body.velocity.y = vec * Math.sin(angle);
                    this.bullet.angle = (angle / Math.PI) * 180;
                    this.bullet.body.velocity.x = vec * Math.cos(angle);
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    this.bullet.body.angularVelocity = 500;
                } else {
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    if (this.player.scale.x == 1) {
                        this.bullet.body.velocity.x = vec;
                        this.bullet.angle = 0;

                    }
                    if (this.player.scale.x == -1) {
                        this.bullet.body.velocity.x = -vec;
                        this.bullet.angle = 180;
                    }
                }
            }
        }
    }
}

/// needle
class Needle extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 1;
        ///
        this.arms = this.player.addChild(
            game.make.sprite(-15, 10, "weapon8")
        );
        this.arms.anchor;
        this.arms.scale.setTo(0.5, 0.5);
        this.arms.enableBody = true;
        this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        ///
        this.pixel = this.game.add.sprite(100, 100, "pixel");
        this.pixel.width = 100;
        this.pixel.height = 100;
        this.pixel.visible = false;
        this.pixel.kill();
        this.game.physics.enable(this.pixel, Phaser.Physics.ARCADE);
        this.game.physics.enable(this.arms, Phaser.Physics.ARCADE);

        this.bulletTime = 0;
        //this.explo = this.game.add.sprite(this.player.x, this.player.y, 'cut');
        this.explo = this.player.addChild(game.make.sprite(-15, 10, "stick"));
        this.explo.anchor.setTo(0.5, 0.5);
        this.explo.scale.setTo(1.5, 2);
        //this.explo.animations.add('cut',[7,6,5,4,3,2,1,0,8],24,false);
        this.explo.animations.add("cut", [0, 1, 2, 3, 4, 5, 6, 7, 8], 48, false);
        this.explo.kill();

        this.bullet = this.pixel;
        this.bullet.wraper = this;
    }
    attack() {
        if (this.game.time.now > this.bulletTime) {
            // if (this.player.scale.x == 1) {
            //     this.pixel.reset(this.player.x + 50, this.player.y - 100);
            // } else {
            //     this.pixel.reset(this.player.x - 150, this.player.y - 100);
            // }
            // this.pixel.visible = false;
            this.bulletTime = this.game.time.now + 1000;
            this.game.add
                    .tween(this.arms)
                    .to({
                            x: 50
                        },
                        100
                    )
                    .to({
                            x: 0
                        },
                        50
                    )
                    .start();
            var set = function (explo, game, needle, pixel, player) {
                if (player.scale.x == 1) {
                    pixel.reset(player.x - 20, player.y - 80);
                } else {
                    pixel.reset(player.x - 130, player.y - 80);
                }
                pixel.visible = false;
                explo.reset(40, 20);
                

                
                explo.animations.play("cut");
                explo.animations.currentAnim.onComplete.add(function () {
                    explo.kill();
                }, explo);
            };
            setTimeout(
                set.bind(
                    "undefined",
                    this.explo,
                    this.game,
                    this.arms,
                    this.pixel,
                    this.player
                ),
                150
            );
        }
    }
}

// /// Icepike
// class IcePike extends Weapons {
//     constructor(type, game, player, enemy) {
//         super(type, game, player, enemy);
//         this.create();
//     }
//     create() {
//         this.damage = 5;
//         this.frequency = 2;
//         this.accuation = 1;
//         this.energy = 0;
//         ///
//         this.arms = this.player.addChild(
//             game.make.sprite(-25, -0, "weapon7")
//         );
//         this.arms.anchor;
//         this.arms.scale.setTo(0.5, 0.5);
//         this.arms.enableBody = true;
//         this.arms.physicsBodyType = Phaser.Physics.ARCADE;
//         ///
//         this.pixel = this.game.add.sprite(100, 100, "pixel");
//         this.pixel.width = 100;
//         this.pixel.height = 100;
//         this.pixel.visible = false;
//         this.pixel.kill();
//         this.game.physics.enable(this.pixel, Phaser.Physics.ARCADE);
//         this.game.physics.enable(this.arms, Phaser.Physics.ARCADE);

//         this.bulletTime = 0;
//         //this.explo = this.game.add.sprite(this.player.x, this.player.y, 'cut');
//         this.explo = this.player.addChild(game.make.sprite(-25, -0, "cut"));
//         this.explo.anchor.setTo(0.5, 0.5);
//         this.explo.scale.setTo(0.5, 0.5);
//         //this.explo.animations.add('cut',[7,6,5,4,3,2,1,0,8],24,false);
//         this.explo.animations.add("cut", [0, 1, 2, 3], 24, false);
//         this.explo.kill();

//         this.bullet = this.pixel;
//         this.bullet.wraper = this;
//     }
//     attack() {
//         if (this.game.time.now > this.bulletTime) {
//             // if (this.player.scale.x == 1) {
//             //     this.pixel.reset(this.player.x + 50, this.player.y - 100);
//             // } else {
//             //     this.pixel.reset(this.player.x - 150, this.player.y - 100);
//             // }
//             // this.pixel.visible = false;
//             this.bulletTime = this.game.time.now + 1000;
//             var set = function (explo, game, needle, pixel, player) {
//                 if (player.scale.x == 1) {
//                     pixel.reset(player.x - 20, player.y - 80);
//                 } else {
//                     pixel.reset(player.x - 130, player.y - 80);
//                 }
//                 pixel.visible = false;
//                 explo.reset(20, 0);
//                 game.add
//                     .tween(needle)
//                     .to({
//                             x: 30
//                         },
//                         100
//                     )
//                     .to({
//                             x: 0
//                         },
//                         50
//                     )
//                     .start();
//                 explo.animations.play("cut");
//                 explo.animations.currentAnim.onComplete.add(function () {
//                     explo.kill();
//                 }, explo);
//             };
//             setTimeout(
//                 set.bind(
//                     "undefined",
//                     this.explo,
//                     this.game,
//                     this.arms,
//                     this.pixel,
//                     this.player
//                 ),
//                 0
//             );
//         }
//     }
// }

///
class IcePike extends Weapons {
    constructor(type, game, player, enemy) {
        super(type, game, player, enemy);
        this.create();
    }
    create() {
        // weapon ability
        this.damage = 5;
        this.frequency = 2;
        this.accuation = 1;
        this.energy = 0;
        ///
        this.arms = this.player.addChild(game.make.sprite(8, 16, "weapon7"));
        //this.arms.physicsBodyType = Phaser.Physics.ARCADE;
        this.arms.scale.setTo(0.5, 0.5);
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(100, "weapon7");
        this.bullets.setAll("anchor.x", 0.5);
        this.bullets.setAll("anchor.y", 0.5);
        this.bullets.setAll("outOfBoundsKill", true);
        this.bullets.setAll("checkWorldBounds", true);
        this.bulletTime = 0;

        //
    }
    attack() {
        //console.log("attack",this.we);
        if (this.game.time.now > this.bulletTime) {
            //  Grab the first bullet we can from the pool
            this.bullet = this.bullets.getFirstExists(false);

            if (this.bullet) {
                //  And fire it
                this.bullet.wraper = this;
                this.bullet.reset(this.player.x, this.player.y + 8);
                this.bullet.lifespan = 3000;
                this.bullet.scale.setTo(0.5, 0.5);
                //get enemy
                var vec = 1000;
                var enemy = this.getenemy(
                    this.player.x,
                    this.player.y,
                    500,
                    this.enemy[0]
                );
                //enemy=null;
                this.game.add
                    .tween(this.arms)
                    .to({
                            angle: -90
                        },
                        50
                    )
                    .yoyo(true, 100)
                    .start();
                if (enemy) {
                    //calculate angle between enemy and player;
                    var angle = game.math.angleBetween(
                        this.player.x,
                        this.player.y,
                        enemy.x,
                        enemy.y
                    );
                    this.bullet.body.velocity.y = vec * Math.sin(angle);
                    this.bullet.angle = (angle / Math.PI) * 180;
                    this.bullet.body.velocity.x = vec * Math.cos(angle);
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    //this.bullet.body.angularVelocity = 500;
                } else {
                    this.bulletTime = this.game.time.now + 500 / this.frequency;
                    if (this.player.scale.x == 1) {
                        this.bullet.body.velocity.x = vec;
                        this.bullet.angle = 0;

                    }
                    if (this.player.scale.x == -1) {
                        this.bullet.body.velocity.x = -vec;
                        this.bullet.angle = 180;
                    }
                }
            }
        }
    }
}
