PI = 3.1415926;

var playTestState = {
    preload: function() {
        game.stage.backgroundColor = "#000000";
        game.load.image("ground", "assets/ground.png");
        this.cursor = game.input.keyboard.createCursorKeys();
        console.log("enter test state");
    },

    create: function() {
        // initialize the map
        game.global.score = 0;
        this.seeBoss = false;

        this.map = game.add.sprite(0, 0, "defaultBg");
        this.map.anchor.setTo(0, 0);
        this.map.width = game.width;
        this.map.height = game.height;

        // initialize the groups
        this.playerGroup = game.add.group();
        this.playerGroup.enableBody = true;

        this.bulletGroup = game.add.group();
        this.bulletGroup.enableBody = true;
        this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup.createMultiple(800, "enemyArrow");

        this.wallGroup = game.add.group();
        this.wallGroup.enableBody = true;
        this.wallGroup.physicsBodyType = Phaser.Physics.ARCADE;

        // initialize the keys
        this.attackKey = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.missileKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
        this.volumeUp = game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.volumeDown = game.input.keyboard.addKey(Phaser.Keyboard.S);

        // initialize the scores
        this.scoreLabel = game.add.text(
            500,
            15,
            "Score: " + game.global.score,
            {
                font: "18px Arial",
                fill: "#ffffff"
            }
        );
        this.hpLabel = game.add.text(30, 15, "Life", {
            font: "18px Arial",
            fill: "#ffffff"
        });
        this.mpLabel = game.add.text(30, 55, "Power", {
            font: "18px Arial",
            fill: "#ffffff"
        });
        this.volumeLabel = game.add.text(300, 15, "Volume", {
            font: "18px Arial",
            fill: "#ffffff"
        });

        // initialize the bgm of the game
        this.bgm_game = game.add.audio("opening");

        // initialize the sprite
        this.player = game.add.sprite(
            game.width / 2,
            game.height / 2,
            game.global.player,
            2,
            (group = this.playerGroup)
        );
        this.player.anchor.setTo(0.5, 0.5);
        this.player.scale.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.player);


        this.volumeBar = game.add.sprite(328, 63, "progressBar");
        this.volumeBar.anchor.setTo(0, 0);
        this.volumeBar.scale.setTo(0.3, 0.3);

        this.lifeBar = game.add.sprite(30, 40, "progressBar");
        this.lifeBar.anchor.setTo(0, 0);
        this.lifeBar.scale.setTo(0.5, 0.3);
        this.powerBar = game.add.sprite(30, 80, "progressBar");
        this.powerBar.anchor.setTo(0, 0);
        this.powerBar.scale.setTo(0.5, 0.3);
        this.lifeVal = 1.0;
        this.powerVal = 1.0;

        // Add floor
        this.floorBottom = game.add.sprite(
            0,
            game.height,
            "progressBar",
            0,
            this.wallGroup
        );
        this.floorTop = game.add.sprite(0, 0, "progressBar", 0, this.wallGroup);
        this.floorLeft = game.add.sprite(
            0,
            0,
            "progressBar",
            0,
            this.wallGroup
        );
        this.floorRight = game.add.sprite(
            game.width,
            0,
            "progressBar",
            0,
            this.wallGroup
        );
        this.floorTop.height = 0;
        this.floorLeft.height = game.height;
        this.floorLeft.width = 0;
        this.floorRight.width = 0;
        this.floorRight.height = game.height;
        this.wallGroup.setAll("body.immovable", true);

        this.bgm_game.play();
        this.changevolume();
        // this.bgm_game.volume = game.global.volume;

        this.weaponLevel = 0;
        this.difficultLevel = 0;

        this.x = new enemyObj(
            "enemy1",
            10,
            1,
            "weapon2",
            "enemyIcePulse",
            undefined,
            game,
            (20, 20)
        );
    },

    update: function() {
        this.x.attack(undefined)
        game.physics.arcade.collide(this.player, this.wallGroup);

        if (!this.player.inWorld) {
            this.playerDie();
        }

        if (game.global.score % 10 == 0 && game.global.score > 0) {
            this.upgradeWeaponChance();
            this.helperChance();
        }
        this.movePlayer();

        if (this.attackKey.isDown) {
            this.playerAttack();
        }
        if (this.missileKey.justPressed()) {
            this.playerMissile();
        }
        if (this.volumeUp.isDown) {
            this.changevolume("+");
        } else if (this.volumeDown.isDown) {
            this.changevolume("-");
        }
    },

    playerAttack: function() {
        this.playerAttackLevel1();
    },

    playerAttackLevel1: function(offset_ = 0, deg = 0) {
        var bullet = this.bulletGroup.getFirstDead();
        var VELOCITY = -800;
        if (!bullet) {
            // console.log('no dead bullets');
            return;
        }
        bullet.anchor.setTo(0.5, 1);
        bullet.reset(this.player.x + offset_, this.player.y);
        bullet.angle = deg;
        bullet.body.velocity.y = VELOCITY * Math.cos((deg / 180) * Math.PI);
        bullet.body.velocity.x = -VELOCITY * Math.sin((deg / 180) * Math.PI);
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;
    },

    playerHurt: function(player, bullet) {
        this.changeLife(-0.01);
        bullet.kill();
    },

    playerDie: function() {
        this.bgm_game.stop();
        var person = prompt("Game Over!! Please enter your name", "player");
        if (person != null) {
            firebase
                .database()
                .ref("leader_board")
                .push({
                    name: person,
                    scores: game.global.score
                });
        }
        var r = confirm("Play again?");
        if (r) {
            game.state.start("play");
        } else {
            game.state.start("menu");
        }
    },

    movePlayer: function() {
        if (this.cursor.left.justDown) {
            this.player.body.velocity.x = -350;
        } else if (this.cursor.right.justDown) {
            this.player.body.velocity.x = 350;
        } else if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -350;
        } else if (this.cursor.right.isDown) {
            this.player.body.velocity.x = 350;
        } else if (this.cursor.left.justUp) {
            this.player.body.velocity.x = 0;
        } else if (this.cursor.right.justUp) {
            this.player.body.velocity.x = 0;
        } else if (this.cursor.up.isDown) {
            this.player.body.velocity.y = -350;
        } else if (this.cursor.down.isDown) {
            this.player.body.velocity.y = 350;
        } else {
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
        }
    },

    changevolume: function(op = undefined) {
        if (op == "+") {
            if (game.global.volume + 0.005 > 1) {
                return;
            }
            game.global.volume += 0.005;
            this.bgm_game.volume = game.global.volume;
        } else if (op == "-") {
            if (game.global.volume - 0.005 < 0) {
                return;
            }
            game.global.volume -= 0.005;
            this.bgm_game.volume = game.global.volume;
        } else {
            this.bgm_game.volume = game.global.volume;
        }
    },

    changeLife: function(val) {
        if (this.lifeVal + val < 0) {
            this.playerDie();
            return false;
        }
        this.lifeVal += val;
        this.lifeBar.scale.setTo(0.5 * this.lifeVal, 0.3);
        return true;
    },

    changePower: function(val) {
        if (this.powerVal + val < 0) {
            console.log(this.powerVal);
            return false;
        } else if (this.powerVal + val > 1) {
            return false;
        }
        this.powerVal += val;
        this.powerBar.scale.setTo(0.5 * this.powerVal, 0.3);
        return true;
    },
};
