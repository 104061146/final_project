#  SS Final Project Proposal
## Motivation
Since we love **Game of Thrones**, we want to create a game for it.

## User Story
- In this game, you are a knight. You need to use the weapon to kill white-walkers.
- There are some weapons on the map, and you can switch the weapon.
- Bird-view
- Player can get experience and money as it kill white-walker. 

## Background
The White Walkers are an otherworldly group of ice creatures preparing to attack Westeros from the far north. They have the ability to reanimate human corpses. Most characters on the show are unaware that they even exist and believe they are creatures of myth. Only the Night's Watch and the wildlings have encountered them so far.

"Winter is coming”. The white walker will break the wall and attack the seven kingdom. You are a character need to save the seven kingdom, to encounter the white walker.

## Instruction
- Move the character with direction key
- Use space to attack the white walkers

## High Risk Analysis
- Weapon system
- Generate random map
- Monster 

## High Value Analysis
- Characters selection 
- Weapon Synthesis System
- Boss