# Software Studio Final Project
> proposal side: [[link]](https://docs.google.com/presentation/d/1pvN53QBo9f-Trc_QvWiJpR7K7Lmxk14QJ3Lq4-if4A4/edit?usp=sharing)    
> proposal spec: [[link]](./proposal.md)  
> gitlab page: [[link]](https://104061146.gitlab.io/final_project)  
> firebase page: [TBD]  
> proposal date: 2019.5.9  
> project demo day 2019.6.18

**Note**: Don't upload firebase related files to this repo. This repo should be put upder `public/` for yout firebase project.

## States
![state_diagram.png](state_diagram.png)

## Characters
<table style="width:100%">
  <tr>
    <td>Sprit</td>
    <td><img src="./assets/characters/paladin.png"></td>
    <td><img src="./assets/characters/elf.png"></td>
    <td><img src="./assets/characters/assassin.png"></td>
    <td><img src="./assets/characters/wizard.png"></td>
  </tr>
  <tr>
    <td>Name</td>
    <td>Jon Snow</td>
    <td>Daenerys <br>Targaryen</td>
    <td>Arya Stark</td>
    <td>Melisandre</td>
  </tr>
  <tr>
    <td>Sprit Name</td>
    <td><code>paladin.png</code></td>
    <td><code>elf.png</code></td>
    <td><code>assassin.png</code></td>
    <td><code>wizard.png</code></td>
  </tr>
</table>

## NPC
- Varys
- Tyrion Lannister
- Bran Stark

## Enemies

## Weapons